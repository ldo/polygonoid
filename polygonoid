#!/usr/bin/python3
#+
# Generate some polygon-like shapes in both straight and curved forms.
#-

import sys
import os
import math
try :
    # available in Python 3.5 or later
    from math import \
        gcd
except ImportError :
    # no longer available in Python 3.9 or later
    from fractions import \
        gcd
#end try
import subprocess
import getopt
import qahirah as qah
from qahirah import \
    CAIRO, \
    Colour, \
    Vector

outfilename = None
figures = []
pix_size = 256
bend = 0
corner_radius = 0
line_width = 2
margin = 10
opts, args = getopt.getopt \
  (
    sys.argv[1:],
    "",
    ["bend=", "corner=", "margin=", "out=", "size=", "thick="]
  )
for keyword, value in opts :
    if keyword == "--bend" :
        bend = float(value)
    elif keyword == "--corner" :
        corner_radius = float(value)
    elif keyword == "--margin" :
        margin = float(value)
    elif keyword == "--out" :
        outfilename = value
    elif keyword == "--size" :
        pix_size = int(value)
        if pix_size < 1 :
            getopt.GetoptError("--size value must be positive integer")
        #end if
    elif keyword == "--thick" :
        line_width = float(value)
    #end if
#end for
if len(args) == 0 :
    raise getopt.GetoptError("no figures to draw")
#end if
for arg in args :
    items = arg.split(":")
    figparms = items[0]
    items = items[1:]
    figparms = tuple(int(i) for i in figparms.split(",", 1))
    if figparms[0] < 2 or figparms[1] == 0 :
        raise getopt.GetoptError \
          (
            "invalid «nr-sides» or «step-size» value for figure spec %s" % repr(arg)
          )
    #end if
    figure = \
        {
            "nr_sides" : figparms[0],
            "step_size" : figparms[1],
            "bend" : bend,
            "corner_radius" : corner_radius,
            "line_width" : line_width,
        }
    for item in items :
        keyword, value = item.split("=", 1)
        if keyword == "bend" :
            figure["bend"] = float(value)
        elif keyword == "corner" :
            figure["corner_radius"] = float(value)
        elif keyword == "thick" :
            figure["line_width"] = float(value)
        else :
            raise getopt.GetoptError \
              (
                "unrecognized keyword %s in figure spec %s" % (repr(keyword), repr(arg))
              )
        #end if
    #end for
    figures.append(figure)
#end for

nr_cols = math.ceil(math.sqrt(len(figures)))
nr_rows = math.ceil(len(figures) / nr_cols)

figure_step = \
    (
        Vector(1, 1)
    *
        round
          (
                pix_size
            +
                2 * max(abs(f["corner_radius"]) for f in figures)
            +
                2 * margin
          )
    )
pix = qah.ImageSurface.create \
  (
    format = CAIRO.FORMAT_RGB24,
    dimensions = Vector(nr_cols, nr_rows) * figure_step
  )
ctx = \
    (qah.Context.create(pix)
        .set_source_colour(Colour.grey(1))
        .paint()
        .set_source_colour(Colour.grey(0))
    )

row = 0
col = 0
for figure in figures :
    (ctx
        .save()
        .translate
          (
                Vector(col, row) * figure_step
            +
                Vector(1, 1) * (margin + abs(figure["corner_radius"]) + pix_size / 2)
          )
        .set_line_width(figure["line_width"])
        .new_path()
    )
    repeat = gcd(figure["nr_sides"], figure["step_size"])
    nr_sides = figure["nr_sides"] // repeat
    step_size = figure["step_size"] // repeat
    bend = figure["bend"]
    corner_radius = figure["corner_radius"]
    flipped = step_size > nr_sides / 2
    for j in range(repeat) :
        ctx.save()
        ctx.rotate(j / repeat * qah.circle / nr_sides - 0.25 * qah.circle)
          # subtract ¼-circle so pattern starts from top (negative-y)
          # rather than side (positive-x)
        for i in range(nr_sides) :
            Θ0 = (i * step_size % nr_sides) * qah.circle / nr_sides
            Θ1 = (i + 1) * step_size % nr_sides * qah.circle / nr_sides
            q0 = Vector.from_polar(pix_size / 2, Θ0)
            q1 = Vector.from_polar(pix_size / 2, Θ1)
            𝜙 = (0.25 - 0 / nr_sides) * qah.circle * (1, -1)[flipped]
            p0 = q0 + Vector.from_polar(corner_radius, Θ0)
            p6 = q1 + Vector.from_polar(corner_radius, Θ1)
            # Note collinearity between control points to ensure smooth curve joins:
            # p0-p1 with p5-p6 from previous segment
            # p2-p3 with p3-p4
            p1 = p0 + Vector.from_polar(corner_radius, Θ0 + 𝜙)
            p5 = p6 + Vector.from_polar(corner_radius, Θ1 - 𝜙)
            p2 = p0 + (p6 - p0).rotate(bend) * 0.5
            p4 = p6 + (p0 - p6).rotate(- bend) * 0.5
            p3 = (p2 + p4) / 2
            p2 = p3 + (q0 - q1) * 0.33
            p4 = p3 + (q1 - q0) * 0.33
            if i == 0 :
                ctx.move_to(p0)
            #end if
            if False : # debug
                ctx.line_to(p1)
                ctx.line_to(p2)
                ctx.line_to(p3)
                ctx.line_to(p4)
                ctx.line_to(p5)
                ctx.line_to(p6)
            else :
                ctx.curve_to(p1, p2, p3)
                ctx.curve_to(p4, p5, p6)
            #end if
        #end for
        ctx.close_path() # unneeded
        ctx.stroke()
        ctx.restore()
    #end for
    ctx.restore()
    col += 1
    if col == nr_cols :
        row += 1
        col = 0
    #end if
#end for

pix.flush()
if outfilename != None :
    pix.write_to_png(outfilename)
else :
    display = subprocess.Popen \
      (
        args = ("display",),
        stdin = subprocess.PIPE
      )
    pix.write_to_png_file(display.stdin)
    display.stdin.close()
    display.wait()
#end if
